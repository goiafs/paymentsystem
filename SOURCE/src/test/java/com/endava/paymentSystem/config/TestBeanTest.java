package com.endava.paymentSystem.config;

import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;

@SpringBootTest
@Disabled
@ActiveProfiles("test")
class TestBeanTest {

    private static final Logger log = LoggerFactory.getLogger(HelloBean.class);

    @Test
    void print() {
        log.info("print from test file");
    }
}