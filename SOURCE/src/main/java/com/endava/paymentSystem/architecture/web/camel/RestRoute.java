package com.endava.paymentSystem.architecture.web.camel;

import org.apache.camel.LoggingLevel;
import org.apache.camel.builder.RouteBuilder;
import org.apache.camel.model.rest.RestBindingMode;
import org.springframework.stereotype.Component;

@Component
public class RestRoute extends RouteBuilder {
    @Override
    public void configure() {

        restConfiguration()
            .component("servlet")
            .bindingMode(RestBindingMode.json);

        rest()
            .get("/hello")
            .to("direct:hello")
            .get("/toFile")
            .to("file:data/outbox?fileName=fromRest.txt");

        from("direct:hello")
            .log(LoggingLevel.INFO, "Hello World")
            .transform().simple("Hello World");

        from("direct:toFile")
            .log(LoggingLevel.INFO, "Hello World")
            .transform().simple("Hello World");
    }
}
