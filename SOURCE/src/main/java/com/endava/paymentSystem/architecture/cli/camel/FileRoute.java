package com.endava.paymentSystem.architecture.cli.camel;

import org.apache.camel.builder.RouteBuilder;
import org.springframework.stereotype.Component;

@Component
public class FileRoute extends RouteBuilder {

    @Override
    public void configure() throws Exception{
        from("file:data/inbox?noop=true")
            .log("Copy files from inbox to outbox")
            .to("file:data/outbox?fileName=all.txt&fileExist=append");
    }
}
