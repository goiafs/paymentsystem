package com.endava.paymentSystem.architecture.web.camel;

import com.endava.paymentSystem.domain.dto.UserDto;
import com.endava.paymentSystem.domain.mapper.UserMapper;
import com.endava.paymentSystem.service.UserService;
import com.endava.paymentSystem.service.impl.UserServiceImpl;
import org.apache.camel.LoggingLevel;
import org.apache.camel.builder.RouteBuilder;
import org.apache.camel.model.rest.RestBindingMode;
import org.springframework.data.annotation.Reference;
import org.springframework.stereotype.Component;

@Component
public class UserRestRoute extends RouteBuilder {

    @Override
    public void configure() throws Exception {
        restConfiguration()
            .component("servlet")
            .bindingMode(RestBindingMode.json);

        rest("/users").consumes("application/json").produces("application/json")
            .get()
            .outType(UserDto.class)
            .route()
            .to("bean:userServiceImpl?method=getAllUsers");


//            .get("/{id}")
//            .outType(UserDto.class)
//            .route()
//            .bean(UserService.class, "getUserById(${header.id})")
//            .endRest();

//            .post()
//            .outType(UserDto.class)
//            .route()
//            .bean(UserService.class, "addNewUser()")
//            .endRest()
//
//            .put("/{id}")
//            .outType(UserDto.class)
//            .route()
//            .bean(UserService.class, "editUser(${header.id})")
//            .endRest()
//
//            .delete()
//            .to("");

        from("direct:users")
            .log("GET /users request received!")
            .to("bean:userService?method=getAllUsers");

//        from("direct:createUser")
//            .log("POST /user/ request received!")
//            .to("bean:usersBean?method=createUser");
//
//        from("direct:deleteUser")
//            .log("DELETE /user/${header.id} request received!")
//            .setBody(simple("${header.id}"))
//            .to("bean:usersBean?method=deleteUser");
    }
}
