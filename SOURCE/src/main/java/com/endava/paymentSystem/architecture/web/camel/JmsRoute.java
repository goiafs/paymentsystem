package com.endava.paymentSystem.architecture.web.camel;

import org.apache.camel.builder.RouteBuilder;
import org.springframework.stereotype.Component;

@Component
public class JmsRoute extends RouteBuilder {

    @Override
    public void configure() throws Exception {
        from("file:data/inbox")
            .log("Send data from file to inputQueue")
            .to("jms:inputQueue");
        from("jms:inputQueue")
            .log("New message received")
            .to("file:data/outbox?fileName=fromJms.txt&fileExist=append");
    }
}
