package com.endava.paymentSystem.service;

import com.endava.paymentSystem.domain.dto.UserDto;

import java.util.List;
import java.util.UUID;

public interface UserService {

    List<UserDto> getAllUsers();

    UserDto getUserById(final UUID id);

    UserDto addNewUser(final UserDto newUser);

    UserDto editUser(final UserDto newUser, final UUID id);

    UserDto deleteUser(final UUID id);
}
