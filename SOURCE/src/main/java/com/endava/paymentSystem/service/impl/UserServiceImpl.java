package com.endava.paymentSystem.service.impl;

import com.endava.paymentSystem.domain.dto.UserDto;
import com.endava.paymentSystem.domain.mapper.UserMapper;
import com.endava.paymentSystem.domain.model.User;
import com.endava.paymentSystem.domain.repository.UserRepository;
import com.endava.paymentSystem.service.UserService;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import javax.xml.ws.http.HTTPException;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

@Service
@RequiredArgsConstructor
public class UserServiceImpl implements UserService {

    private final UserMapper mapper;
    private final UserRepository repository;

    @Override
    public List<UserDto> getAllUsers() {
        final List<UserDto> users = new ArrayList<>();
        repository.findAll().forEach(user -> users.add(mapper.mapToDto(user)));
        return users;
    }

    @Override
    public UserDto getUserById(final UUID id) {
        final User user = repository.findById(id).orElseThrow(() -> new HTTPException(404));
        return mapper.mapToDto(user);
    }

    @Override
    public UserDto addNewUser(final UserDto newUser) {
        final User user = mapper.mapToEntity(newUser);
        repository.save(user);

        return mapper.mapToDto(user);
    }

    @Override
    public UserDto editUser(final UserDto newUser, final UUID id) {
        final User oldUser = mapper.mapToEntity(getUserById(id));
        User editedUser = mapper.mapToEntity(newUser);

        editedUser.set_id(oldUser.get_id());
        repository.save(editedUser);

        return mapper.mapToDto(editedUser);
    }

    @Override
    public UserDto deleteUser(final UUID id) {
        final User oldUser = mapper.mapToEntity(getUserById(id));
        repository.delete(oldUser);

        return mapper.mapToDto(oldUser);
    }
}
