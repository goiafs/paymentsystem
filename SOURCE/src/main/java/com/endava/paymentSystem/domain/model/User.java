package com.endava.paymentSystem.domain.model;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import java.util.UUID;

@Getter
@Setter
@NoArgsConstructor
@Entity
public class User {

    @Id
    private UUID _id;

    @Column
    private String fistName;

    @Column
    private String lastName;

    @Column
    private String email;
}
