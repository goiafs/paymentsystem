package com.endava.paymentSystem.domain.mapper;

import com.endava.paymentSystem.domain.dto.UserDto;
import com.endava.paymentSystem.domain.model.User;
import org.springframework.stereotype.Component;

@Component
public class UserMapper {

    public UserDto mapToDto(final User user) {
        final UserDto dto = new UserDto();

        dto.setId(user.get_id());
        dto.setFistName(user.getFistName());
        dto.setLastName(user.getLastName());
        dto.setEmail(user.getEmail());

        return dto;
    }

    public User mapToEntity(final UserDto dto) {
        final User user = new User();

        user.set_id(dto.getId());
        user.setFistName(dto.getFistName());
        user.setLastName(dto.getLastName());
        user.setEmail(dto.getEmail());

        return user;
    }
}
