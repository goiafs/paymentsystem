package com.endava.paymentSystem.config;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;

@Component
@Profile("test")
public class TestBean {
    private static final Logger log = LoggerFactory.getLogger(HelloBean.class);

    @PostConstruct
    void postConstruct() {
        log.info("loaded TestBean!");
    }
}
