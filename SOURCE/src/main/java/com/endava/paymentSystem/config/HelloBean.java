package com.endava.paymentSystem.config;

import lombok.extern.slf4j.Slf4j;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

@Component
@Slf4j
public class HelloBean {

    private static final Logger log = LoggerFactory.getLogger(HelloBean.class);

    HelloBean(@Value("${helloMessage}") String helloMessage) {
        log.info(helloMessage);
    }
}
